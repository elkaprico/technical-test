<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TaxTest extends TestCase
{
    use WithFaker;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testPageIsWorking()
    {
        $response = $this->get(route('tax.index'));

        $response->assertStatus(200);
    }

    public function testInputRequestSuccess()
    {
        $status = config('tax.relief');
        $randStatus = array_rand($status);
        $randIncome = $this->faker->numberBetween(3000000, 300000000);
        $request =  [
            'name' => $this->faker->name,
            'status' => $randStatus,
            'income' => $randIncome,
            '_token' => csrf_token()
        ];

        // start internal checking
        $relief = $status[$randStatus]['value'];
        $yearlyIncome = $request['income'] * 12;
        $netIncome = $yearlyIncome - $relief;
        $taxScheme = config('tax.scheme');
        $tmp = [];
        $scheme = [];
        foreach ($taxScheme as $key => $value) {
            $tmp[$value['value']][] = $key;
        }
        // sort key by ASC
        ksort($tmp);
        foreach ($tmp as $val) {
            foreach ($val as $key => $value) {
                $scheme[] = $taxScheme[$value];
            }
        }
        $this->taxCalculation($scheme, $netIncome, 0, $tax);
        // end internal checking

        $response = $this->post(route('tax.index'), $request);
        $responseTotalTax = session()->get('result')['totalTax'];
        $this->assertEquals($responseTotalTax, $tax);
        $response->assertStatus(302);
        $response->assertRedirect(route('tax.index'));
    }

    public function testEmptyRequestInputIncome()
    {
        $status = config('tax.relief');
        $randStatus = array_rand($status);
        $request =  [
            'name' => $this->faker->name,
            'status' => $randStatus,
            '_token' => csrf_token()
        ];
        $response = $this->getResponse($request);
        $this->getErrorRequired('income');
        $response->assertStatus(302);
        $response->assertSessionHasErrors('income');
    }

    public function testEmptyRequestInputName()
    {
        $status = config('tax.relief');
        $randStatus = array_rand($status);
        $randIncome = $this->faker->numberBetween(3000000, 300000000);
        $request =  [
            'status' => $randStatus,
            'income' => $randIncome,
            '_token' => csrf_token()
        ];
        $response = $this->getResponse($request);
        $this->getErrorRequired('name');
        $response->assertStatus(302);
        $response->assertSessionHasErrors('name');
    }

    public function testEmptyRequestInputStatus()
    {
        $randIncome = $this->faker->numberBetween(3000000, 300000000);
        $request =  [
            'name' => $this->faker->name,
            'income' => $randIncome,
            '_token' => csrf_token()
        ];
        $response = $this->getResponse($request);
        $this->getErrorRequired('status');
        $response->assertStatus(302);
        $response->assertSessionHasErrors('status');
    }

    public function testNotInRequestInputStatus()
    {
        $randIncome = $this->faker->numberBetween(3000000, 300000000);
        $request =  [
            'name' => $this->faker->name,
            'status' => 'xyz',
            'income' => $randIncome,
            '_token' => csrf_token()
        ];
        $response = $this->getResponse($request);
        $this->getErrorInvalid('status');
        $response->assertStatus(302);
        $response->assertSessionHasErrors('status');
    }

    private function getErrorRequired($input = '')
    {
        $errMsg = $this->getErrorFromSession($input);
        $this->assertEquals($errMsg, 'The '.$input.' field is required.');
    }

    private function getErrorInvalid($input = '')
    {
        $errMsg = $this->getErrorFromSession($input);
        $this->assertEquals($errMsg, 'The selected '.$input.' is invalid.');
    }

    private function getResponse($request)
    {
        return $this->post(route('tax.index'), $request);
    }

    private function getErrorFromSession($input)
    {
        $errors = session('errors');
        $msgs = $errors->getBag('default')->getMessages();
        return array_shift($msgs[$input]);
    }

    private function taxCalculation($taxScheme, $netIncome, $inc = 0, &$tax)
    {
        if (isset($taxScheme[$inc])) {
            $curentScheme = $taxScheme[$inc];
            $percent = $curentScheme['percent'];
            $curValue = $curentScheme['value'];
            $befValue = 0;
            if (isset($taxScheme[$inc-1])) {
                $befValue = $taxScheme[$inc-1]['value'];
            }
            $netIncome = $netIncome - ($curValue - $befValue);
            $nextValue = $netIncome;
            if (isset($taxScheme[$inc+1])) {
                $nextValue = $taxScheme[$inc+1]['value'];
            }
            if ($netIncome <= 0) {
                return;
            }
            $taxIncome = $nextValue - $curValue;
            $continue = true;
            if ($netIncome < $taxIncome || $taxIncome <= 0) {
                if ($taxIncome <= 0) {
                    $continue = false;
                }
                $taxIncome = $netIncome;
            }
            $tax += $taxIncome * $percent / 100;
            if ($continue) {
                $inc++;
                return $this->taxCalculation(
                    $taxScheme, $netIncome, $inc, $tax
                );
            }
        }
    }
}
