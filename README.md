# Online Pajak PHP Technical Test
PHP Technical Test

## How to Deploy
__1. Install Git__

Open your console and run this command
```console
  sudo apt-get install git
```

__2. Install Composer__
```console
  cd ~
  curl -sS https://getcomposer.org/installer -o composer-setup.php
  sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer
```

__3. Clone this Repository__
```console
  git clone https://bitbucket.org/elkaprico/technical-test.git && cd technical-test
```
__4. Copy .env File__
```console
  cp .env.example .env
```

__5. Install Composer Project__
```console
  composer install
```

__6. Run Laravel Server__
```console
  php artisan serve
```

__7. Test on your browser__

Open your browser and type http://127.0.0.1:8000

If everything is working, you'll see a welcome page

__8. Launch Tax Program__

Click link `PROGRAM` on welcome page or type `http://127.0.0.1:8000/tax` on your browser, that will redirect to tax program.

If everything is working, you'll see a tax program page. Feel free to test

__9. Change / Add New Schema__

Edit config file in `config/tax.php`

For Relief, change on `relief`
For Scheme, change on `scheme`

__10. Unit Test__

Create new tab on your console, and run this command
```console
  ./vendor/bin/phpunit
```

#### Thank You
