<!doctype html>
<?php
 ?>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>
        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                                <div class="d-flex align-items-center">
                                    <h2>Calculate Tax</h2>
                                </div>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('tax.store') }}" method="post">
@csrf
                                    <div class="form-group row">
                                        <label for="tax-name" class="col-md-5">Name</label>
                                        <div class="col-md-7">
                                            <input type="text" required name="name" id="tax-name" value="{{ old('name') }}" class="form-control {{ $errors->has('name') ? 'is-invalid': '' }}" placeholder="Name">
@if ($errors->has('name'))
                                            <div class="invalid-feedback d-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </div>
@endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="tax-status" class="col-md-5">Marrieage status</label>
                                        <div class="col-md-7">
                                            <select name="status" required id="tax-status" class="form-control {{ $errors->has('status') ? 'is-invalid': '' }}" >
@foreach ($status as $key => $value)
                                                <option value="{{ $key }}">{{ $value['name'] }}</option>
@endforeach
                                            </select>
@if ($errors->has('status'))
                                            <div class="invalid-feedback d-block">
                                                <strong>{{ $errors->first('status') }}</strong>
                                            </div>
@endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="tax-income" class="col-md-5">Income per Month</label>
                                        <div class="col-md-7 input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">RP</span>
                                            </div>
                                            <input type="number" required name="income" id="tax-income" value="{{ old('income') }}" class="form-control {{ $errors->has('income') ? 'is-invalid': '' }}" placeholder="3.000.000">
@if ($errors->has('name'))
                                            <div class="invalid-feedback d-block">
                                                <strong>{{ $errors->first('income') }}</strong>
                                            </div>
@endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-outline-primary btn-lg">Calculate</button>
                                    </div>
                                </form>
@include ('result')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>
</html>
