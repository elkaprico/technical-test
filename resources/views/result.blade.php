@if (session('result'))
    @php
        $sess = session('result');
        $request = $sess['request'];
        $result = $sess['result'];
    @endphp
    <hr/>
    <h3>User</h3>
    <div class="media">
        <div class="d-flex flex-column counters">
            Name : {{ $request['name'] }}
        </div>
    </div>
    <div class="media">
        <div class="d-flex flex-column counters">
            Marrieage status : {{ $request['status']}}
        </div>
    </div>
    <div class="media">
        <div class="d-flex flex-column counters">
            Income per Month : {{ $request['income'] }}
        </div>
    </div>
    <div class="media">
        <div class="d-flex flex-column counters">
            Yearly Income : {{ $request['yearlyIncome'] }}
        </div>
    </div>
    <div class="media">
        <div class="d-flex flex-column counters">
            Non-Taxable Income : {{ $sess['relief'] }}
        </div>
    </div>
    <div class="media">
        <div class="d-flex flex-column counters">
            Taxable Income : {{ $request['taxableIncome'] }}
        </div>
    </div>
    <div>&nbsp;</div>
    <h3>Tax Calculation</h3>
    @foreach ($result as $key => $value)
    <div class="media">
        <div class="d-flex flex-column counters">
            Taxable income lvl {{ $value['taxLevel'] }}: {{ $value['taxIncomeFormat'] }} * {{ $value['percentage'] }}% = {{ $value['taxFormat'] }}
        </div>
    </div>
    @endforeach
    <div class="media">
        <div class="d-flex flex-column counters">
            Total Tax : {{ $sess['tax']['totalFormated'] }}
        </div>
    </div>
@endif
