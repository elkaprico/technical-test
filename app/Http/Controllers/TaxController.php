<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaxRequest;
use App\Providers\TaxProvider;
use Illuminate\Http\Request;

class TaxController extends Controller
{


    protected $result = [];
    protected $tax = 0;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get marriage status as tax relief
        $status = $this->getRelief();
        return view('tax', compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaxRequest $request)
    {
    ///    dd($request->toArray());
        //echo '<pre/>';
        // multiple 12 (year = 12 months)
        $yearlyIncome = $request->income * 12;
        // cut with Relief
        $status = $this->getRelief();
        $relief = $status[$request->status]['value'];
        $netIncome = $yearlyIncome - $relief;
        if ($netIncome > 0) {
            $taxScheme = $this->getScheme();
            $this->taxCalculation($taxScheme, $netIncome);
        } else {
            $netIncome = 0;
        }
        $req = [
            'name' => $request->name,
            'status' => $status[$request->status]['name'],
            'income' => $this->formatedRupiah($request->income),
            'yearlyIncome' => $this->formatedRupiah($yearlyIncome),
            'taxableIncome' => $this->formatedRupiah($netIncome)
        ];
        $result = [
            'request' => $req,
            'totalTax' => $this->tax,
            'tax' => [
                'total' => $this->tax,
                'totalFormated' => $this->formatedRupiah($this->tax),
            ],
            'relief' => $this->formatedRupiah($relief),
            'result' => $this->result
        ];
        return redirect()->route('tax.index')
            ->with('result', $result);
    }

    /**
     * Recursive Function for Tax Calculation
     *
     * @param  Array  $taxScheme
     * @param Int  $netIncome
     * @param Int  $inc = 0
     * @return self
     */
    private function taxCalculation($taxScheme, $netIncome, $inc = 0)
    {
        if (isset($taxScheme[$inc])) {
            $curentScheme = $taxScheme[$inc];
            $percent = $curentScheme['percent'];
            $curValue = $curentScheme['value'];
            /*
             set minimum level
             $nextValue mean floor limit
            */
            $befValue = 0;
            if (isset($taxScheme[$inc-1])) {
                $befValue = $taxScheme[$inc-1]['value'];
            }
            $netIncome = $netIncome - ($curValue - $befValue);
            /*
             set maximum level
             $nextValue mean ceil limit
            */
            $nextValue = $netIncome;
            // its mean maximum (last condition), mean unlimited
            if (isset($taxScheme[$inc+1])) {
                $nextValue = $taxScheme[$inc+1]['value'];
            }
            // no need continue if net income is below 0
            if ($netIncome <= 0) {
                return;
            }
            // set taxIncome = max level - current scheme
            $taxIncome = $nextValue - $curValue;
            /*
                $netIncome < $taxIncome mean it max loop
                $taxIncome <= 0 mean if tax is below 0
                then use $netIncome as $taxIncome
            */
            $continue = true;
            if ($netIncome < $taxIncome || $taxIncome <= 0) {
                // make stopper here, mean this is last loop
                if ($taxIncome <= 0) {
                    $continue = false;
                }
                $taxIncome = $netIncome;
            }
            $tax = $taxIncome * $percent / 100;
            // sett total tax
            $this->tax += $tax;
            $this->result[] = [
                'income' => $netIncome,
                'taxIncome' => $taxIncome,
                'percentage' => $percent,
                'taxLevel' => $inc+1,
                'tax' => $tax,
                'taxIncomeFormat' => $this->formatedRupiah($taxIncome),
                'incomeFormat' => $this->formatedRupiah($netIncome),
                'taxFormat' => $this->formatedRupiah($tax)
            ];
            if ($continue) {
                $inc++;
                return $this->taxCalculation($taxScheme, $netIncome, $inc);
            }
        }
    }

    private function formatedRupiah($arg)
    {
        return 'Rp '.number_format($arg);
    }

    private function getRelief()
    {
        return config('tax.relief');
    }

    private function getScheme()
    {
        // reorder taxScheme, to set if invalid / wrong min max
        $taxScheme = config('tax.scheme');
        $tmp = [];
        $scheme = [];
        foreach ($taxScheme as $key => $value) {
            $tmp[$value['value']][] = $key;
        }
        // sort key by ASC
        ksort($tmp);
        foreach ($tmp as $val) {
            foreach ($val as $key => $value) {
                $scheme[] = $taxScheme[$value];
            }
        }
        return $scheme;
    }
}
