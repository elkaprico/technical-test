<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaxRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $status = config('tax.relief');
        return [
            'name' => 'required|max:255',
            'status' => 'required|in:'.join(',', array_keys($status)),
            'income' => 'required|integer'
        ];
    }
}
